<?php
// Assuming you installed from Composer:
require "vendor/autoload.php";
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);

$pages = array();
$handle = opendir('./pages');
if ($handle) {
    while (($entry = readdir($handle)) !== FALSE) {
        $pages[] = $entry;
    }
}
closedir($handle);

$page = @$_GET['page'] ?: 'home.php';
if (substr($page, -4) !== '.php') {
    $page .= '.php';
}

if (!in_array($page, $pages)) {
    $page = 'home.php';
}
?>
<html>
    <head>
      <title>IMDB zoekmachine</title>
        <link rel="stylesheet" href="./css/skeleton.css"/>
        <link rel="stylesheet" href="./css/normalize.css"/>
<!--        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">-->
<!--        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>-->
<!--        <link rel="stylesheet" href="./css/theme.css"/>-->
        <link rel="stylesheet" href="./css/styling.css"/>
    </head>
    <body>
        <?php include './pages/'.$page; ?>
    </body>
</html>
