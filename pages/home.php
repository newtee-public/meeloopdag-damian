<?php
$search = '';
if (isset($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
}
?>

<h1>IMDB zoekmachine</h1>

<form>
  <label>
    Zoek:
    <input name="search" value="<?= $search ?>">
  </label>
</form>


<?php

use hmerritt\Imdb;

$imdb = new Imdb;

$films = array();
if ($search) {
    $films = $imdb->search($search, ['category' => 'tt'])['titles'];
}

// TODO: verberg tabel als er geen resultaten zijn
// TODO: Extra column
//        - linkje naar IMDB
//        - meer gegevens beschikbaar/zinvol?
// TODO: MAX AANTAL (per pagina)
// Andere opties slim op een overzichtspagina / tabel?
?>

<table>
  <thead>
  <tr>
    <th>Titel</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($films as $film) { ?>
    <tr>
      <td>
        <a href="./movie?filmId=<?= $film['id'] ?>"><?= $film['title'] ?></a><br>
      </td>
    </tr>
  <?php } ?>
  </tbody>
</table>
