<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'hmerritt/imdb-api' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../hmerritt/imdb-api',
            'aliases' => array(),
            'reference' => '39e54f7c089c7cb6d8245404acb5a89bd8caf760',
            'dev_requirement' => false,
        ),
        'paquettg/php-html-parser' => array(
            'pretty_version' => '2.2.1',
            'version' => '2.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paquettg/php-html-parser',
            'aliases' => array(),
            'reference' => '668c770fc5724ea3f15b8791435f054835be8d5e',
            'dev_requirement' => false,
        ),
        'paquettg/string-encode' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paquettg/string-encode',
            'aliases' => array(),
            'reference' => 'a8708e9fac9d5ddfc8fc2aac6004e2cd05d80fee',
            'dev_requirement' => false,
        ),
        'tmarois/filebase' => array(
            'pretty_version' => 'v1.0.24',
            'version' => '1.0.24.0',
            'type' => 'package',
            'install_path' => __DIR__ . '/../tmarois/filebase',
            'aliases' => array(),
            'reference' => 'b660650472362e45f78d97cb395f09889196d164',
            'dev_requirement' => false,
        ),
    ),
);
